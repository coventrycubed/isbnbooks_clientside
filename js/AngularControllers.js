/**
* Created with ISBNclent.
* User: StayGrafting
* Date: 2015-06-28
* Time: 04:54 PM
* To change this template use Tools | Templates.
*/

var isbnClient = angular.module("isbnClient", ['ngRoute']);
isbnClient.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/welcome', {
            templateUrl: 'pages/mainsearch.html'
        }).when('/login', {
            templateUrl: 'pages/loginpage.html'
        }).when('/signup', {
            templateUrl: 'pages/register.html'
        }).when('/logout', {
            templateUrl: 'pages/logout.html'
        }).otherwise({
            redirectTo: '/login'
        })
    }
]);


isbnClient.controller('logControl', [ 'userObject', '$http', '$location', 
        function(userObject, $http, $location){
            var self = this;
             
             this.uname='';
             this.pass = '';
             this.logOn = function(){
                console.log('Log on')
                var upass = this.pass
                //self.docs = {};
                $http({
                    url: 'http://telex-golf.codio.io:3000/user?id='+this.user_name,
                    method: 'GET',
                    withCredentials: false
                   
                }).success(function(data, status, headers, config) {
                    
                    //console.log(data)
                    var passcheck = data.password
                    
                    if(passcheck === upass){
                        //set user object
                        userObject.set(data.email, data.password, data.first_name, data.last_name);
                        console.log(userObject.get());
                        $location.path('/welcome');
                        $location.replace;
                    }else{
                        self.mess='Wrong username / password Combination';
                    }
                    
                }).error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    console.log(data)
                    console.log()
                })
             };
        }
 ]);
//end of login controller

//logout controller
isbnClient.controller('logoutController', ['userObject', '$http', '$location',
        function(userObject, $http, $location) {
            this.logout = function() {
                console.log('log out called')
                userObject.reset();
                $location.path('/login');
                $location.replace;
            }
            this.logout();
        }
    ]);






//registration controller
isbnClient.controller('registerController', ['$http', 
    function($http){
        var self = this;
        this.register = function(){
            //get form values
            var email = self.email;
            var fname = self.fname;
            var lname = self.lname;
            var pass = self.pass;
            var url = 'http://telex-golf.codio.io:3000/adduser?email='+email+'&fname='+fname+'&lname='+lname+'&pass='+pass
            //make http request
            $http({
                url: url,
                method: 'PUT'
            }).success(function(data, status, headers, config){
                console.log(url)
                self.message = 'You have successfully registerd, Thank You';
            }).error(function(data, status, headers, config){
                
            });
        }
    }
                                            
]);
//end of register conttroller


//search controller for handling search function
isbnClient.controller('searchControl', ['userObject', '$http', '$location', 
               function(userObject, $http, $location){
                   var self = this
                   //check for user info
                   var self = this;
                    this.loggedinstate = userObject.get().logged_in;
                    this.loggedinuser = userObject.get().first_name;
                    this.loggedinusername = userObject.get().user_name;
                    var sec = userObject.get().user_name;
                    console.log('----------------');
                    console.log(userObject.get());
                    console.log(this.loggedinstate);
                    console.log(this.loggedinusername);
                    if(this.loggedinstate == false){
                        $location.path('/login');
                        $location.replace;   
                    }


                    this.loginlink = function() {
                        if(userObject.get().logged_in) {
                            return 'logout'
                        } else {
                            return 'login'
                        }
                    }

                    self.user_name = "Hi, "+sec;
                   
                   
                   
                    this.gosearch = function(){
                        var dummy_isbn = '9780321995780'
                        var isbn = this.searchisbn;
                        console.log(isbn)
                        $http({
                            url:'http://telex-golf.codio.io:3000/books?isbn='+isbn,
                            
                            method:'GET'
                        }).success(function(data, status, headers, config){
                            
                            var book_data = data.items[0]; 
                            self.books = []
                            var book ={
                                title: book_data.volumeInfo.title,
                                publisher: book_data.volumeInfo.publisher,
                                date: book_data.volumeInfo.publishedDate,
                                desc: book_data.volumeInfo.description,
                                thumbnail: book_data.volumeInfo.imageLinks.thumbnail
                            };
                            self.book = book;
                            self.books.push(book)
                            console.log(self.books[0].title);
                            
                        }).error(function(){
                            console.log(data)
                            console.log(status)
                            console.log(headers)
                            console.log(config)
                        });
                    }
               }
 ]);


// user factory for userService Object
isbnClient.factory('userObject', [
    

    function(){
        //status of user object
        var status ={
            logged_in: false,
            user_name: '',
            password:'',
            first_name: '',
            last_name: ''
        };

        return {
            get: function(){
                return status;
            },
            
           
            set: function(uname, password, fname, lname){
                status.user_name = uname;
                status.password = password;
                status.first_name = fname;
                status.last_name = lname
                status.logged_in = true;
                return status;
            },
            reset: function() {
                console.log('user service reset ');
                status = {
                    logged_in: false,
                    user_name: '',
                    password:'',
                    first_name: '',
                    last_name: ''
                    
                }
                return status;
            }
        }
    }
    
    
]);
